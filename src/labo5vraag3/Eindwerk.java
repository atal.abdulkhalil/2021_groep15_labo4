package labo5vraag3;

public class Eindwerk implements Comparable<Eindwerk>{
    private String title;
    private Integer jaartal;
    private String opleiding;
    private Student student;

    public Eindwerk(String title, Integer jaartal, String opleiding, Student student) {
        this.title = title;
        this.jaartal = jaartal;
        this.opleiding = opleiding;
        this.student = student;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getJaartal() {
        return jaartal;
    }

    public void setJaartal(Integer jaartal) {
        this.jaartal = jaartal;
    }

    public String getOpleiding() {
        return opleiding;
    }

    public void setOpleiding(String opleiding) {
        this.opleiding = opleiding;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @Override
    public String toString() {
        return "Eindwerk{" +
                "title='" + title + '\'' +
                ", jaartal='" + jaartal + '\'' +
                ", opleiding='" + opleiding + '\'' +
                ", student=" + student +
                '}';
    }

    @Override
    public int compareTo(Eindwerk o) {
        if (! this.getOpleiding().equals(o.getOpleiding()) ){
            return this.getOpleiding().compareTo(o.getOpleiding());
        }

        if(! this.student.getFamilienaam().equals(o.student.getFamilienaam())){
            return this.student.getFamilienaam().compareTo(o.student.getFamilienaam());
        }

        return this.student.getVoornaam().compareTo(o.student.getVoornaam());
    }
}


