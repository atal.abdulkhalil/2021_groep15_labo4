package labo5vraag3;

public class Student {
    private String familienaam;
    private String voornaam;
    private String studentenCode;

    public Student(String familienaam, String voornaam, String studentenCode) {
        this.familienaam = familienaam;
        this.voornaam = voornaam;
        this.studentenCode = studentenCode;
    }

    public String getFamilienaam() {
        return familienaam;
    }

    public void setFamilienaam(String familienaam) {
        this.familienaam = familienaam;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    public String getStudentenCode() {
        return studentenCode;
    }

    public void setStudentenCode(String studentenCode) {
        this.studentenCode = studentenCode;
    }

    @Override
    public String toString() {
        return "Student{" +
                "familienaam='" + familienaam + '\'' +
                ", voornaam='" + voornaam + '\'' +
                ", studentenCode='" + studentenCode + '\'' +
                '}';
    }
}


