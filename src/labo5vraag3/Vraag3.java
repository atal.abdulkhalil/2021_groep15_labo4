package labo5vraag3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toSet;

public class Vraag3 {
    private static ArrayList<Eindwerk> eindwerken = readFile(2000);

    public static void main(String[] args) {
//        supplierEindwerken()
//                .get()
//                .filter( x -> x.getOpleiding().equals("ICT"))
//                .forEach(System.out::println);


        Map<Object, Set<Eindwerk>> eindwerkHashmap = supplierEindwerken()
                .get()
                .filter(o -> o.getTitle().endsWith("t "))
                .collect(groupingBy(o -> o.getOpleiding(), toSet()));

        for (Map.Entry<Object, Set<Eindwerk>> x : eindwerkHashmap.entrySet()){
            System.out.println(x.getKey() + "    " + x.getValue());
        }


    }

    private static Supplier<Stream<Eindwerk>> supplierEindwerken(){
        Supplier<Stream<Eindwerk>> eindwerkSupplier = () -> eindwerken.stream();
        return eindwerkSupplier;
    }

    private static ArrayList<Eindwerk> readFile(int aantal) {
        ArrayList<Eindwerk> eindwerken;
        eindwerken = new ArrayList<Eindwerk>();
        String line = null;

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("eindwerken.txt"))) {
            for (int i = 0; i < aantal; i++) {
                line = bufferedReader.readLine();
                String[] lineParts = line.split("\\$");
                if (lineParts.length > 0) {
                    Student student = new Student(lineParts[0], lineParts[1], lineParts[2]);
                    Eindwerk eindwerk = new Eindwerk(lineParts[3], (Integer.parseInt(lineParts[4])), lineParts[5], student);
                    eindwerken.add(eindwerk);
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return eindwerken;
    }

}
