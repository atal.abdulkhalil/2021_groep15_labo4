package SorteerAlgo;

import java.time.LocalDate;

public class Identity implements Comparable<Identity>{
    private String Naam;
    private String Voornaam;
    private LocalDate GeboorteDatum;
    private String GeboortePlaats;

    public Identity(String naam, String voornaam, LocalDate geboorteDatum, String geboortePlaats) {
        Naam = naam;
        Voornaam = voornaam;
        GeboorteDatum = geboorteDatum;
        GeboortePlaats = geboortePlaats;
    }

    public LocalDate getGeboorteDatum() {
        return GeboorteDatum;
    }

    public String getNaam() {
        return Naam;
    }

    public void setNaam(String naam) {
        Naam = naam;
    }

    public String getVoornaam() {
        return Voornaam;
    }

    public void setVoornaam(String voornaam) {
        Voornaam = voornaam;
    }

    public String getGeboortePlaats() {
        return GeboortePlaats;
    }

    public void setGeboortePlaats(String geboortePlaats) {
        GeboortePlaats = geboortePlaats;
    }


    @Override
    public String toString() {
        return "Identity{" +
                "Naam='" + Naam + '\'' +
                ", Voornaam='" + Voornaam + '\'' +
                ", GeboorteDatum='" + GeboorteDatum + '\'' +
                ", GeboortePlaats='" + GeboortePlaats + '\'' +
                '}';
    }


    @Override
    public int compareTo(Identity o) {
        if (! this.getGeboorteDatum().equals(o.getGeboorteDatum())){
            return this.getGeboorteDatum().compareTo(o.getGeboorteDatum());
        }
        if (! this.getGeboortePlaats().equals(o.getGeboortePlaats())){
            return this.getGeboortePlaats().compareTo(o.getGeboortePlaats());
        }
        if (! this.getNaam().equals(o.getNaam())){
            return this.getNaam().compareTo(o.getNaam());
        }
        return this.getVoornaam().compareTo(o.getVoornaam());
    }
}

