package SorteerAlgo;

import java.util.Arrays;

public class InsertionSort implements ISort{
    @Override
    public void sort(int[] input) {
        for (int i = 1; i < input.length; i++) {
            int key = input[i];
            int j = i - 1;
            while (j >= 0 && input[j] > key) {
                input[j + 1] = input[j];
                j = j - 1;
            }
            input[j + 1] = key;
        }
    }

    @Override
    public void printArray(int[] array) {
        System.out.println(Arrays.toString(array));
    }
}
