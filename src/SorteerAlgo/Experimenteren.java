package SorteerAlgo;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public class Experimenteren {
    public static void main(String[] args){
        int aantal = 1000000;

//        InsertionSort insertionSort = new InsertionSort();
//        BubbleSort bubbleSort = new BubbleSort();
//        MergeSort mergeSort = new MergeSort();
//        QuickSort quickSort = new QuickSort();

//        Comparable[] array = makeIdentities(aantal);
//        long start = System.nanoTime();
//
//        Arrays.sort(array);
//
//        long dur = System.nanoTime() - start;
//
//        System.out.println(aantal + " javasort  " + dur);


        BubbleSortObject bso = new BubbleSortObject();
        long timeBubbleObject = experimentObject(bso, aantal);

        System.out.println(aantal + " bubbelsortObject  " + timeBubbleObject);


//        long time = experiment(insertionSort, aantal);
//        long time2 = experiment(bubbleSort, aantal);
//        long time3 = experiment(mergeSort, aantal);
//        long time4 = experiment(quickSort, aantal);
//
//        System.out.println("insertionSort " + time);
//        System.out.println("bubbleSort " + time2);
//        System.out.println("mergeSort " + time3);
//        System.out.println("quickSort " + time4);
    }

    public static long experiment(ISort algo, int aantal){
        int[] randomArr;
        HelperClass helperKlas = new HelperClass();
        randomArr = helperKlas.generateRandomArr(aantal);

        long startTime =  System.nanoTime();

        algo.sort(randomArr);

        return System.nanoTime() - startTime;
    }

    public static long experimentObject(ISortObject algo, int aantal){
        Comparable[] randomArr = makeIdentities(aantal);
        HelperClass helperKlas = new HelperClass();

        long startTime =  System.nanoTime();

        algo.sortObject(randomArr);

        long endTime =  System.nanoTime();

        printIdentities((Identity[]) randomArr);

        return endTime - startTime;
    }

    public static Identity[] makeIdentities(int aantal){
        HelperClass helperKlas = new HelperClass();

        LocalDate[] data = helperKlas.dateGenerator(aantal);
        List<String> namen = helperKlas.readFile(aantal, "names.txt");
        List<String> voornamen = helperKlas.readFile(aantal, "first-names.txt");
        List<String> places = helperKlas.readFile(aantal, "placesA.txt");

        Identity[] identities= new Identity[aantal];

        for (int i = 0; i < aantal; i++){
            identities[i] = new Identity(namen.get(i), voornamen.get(i), data[i], places.get(i));
        }

        return identities;
    }

    public static void printIdentities(Identity[] ids){
        for (Identity id:ids){
            System.out.println(id);
        }
    }
}


