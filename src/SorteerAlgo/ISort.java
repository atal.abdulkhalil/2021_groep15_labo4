package SorteerAlgo;

public interface ISort {
    public void sort(int[] array);
    public void printArray(int[] array);
}
