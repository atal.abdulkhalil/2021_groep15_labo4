package SorteerAlgo;

import java.util.Arrays;

public class BubbleSortObject implements ISortObject {

    @Override
    public void sortObject(Comparable[] array) {
        int length = array.length;

        for (int i = 0; i < length - 1; i++) {
            for (int j = 0; j < length - i - 1; j++) {

                if (array[j].compareTo(array[j + 1])>0) {

                    Comparable temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }

    @Override
    public void printArrayObject(Comparable[] array) {
        for (Comparable arr:array)
            System.out.println(arr);
    }
}
