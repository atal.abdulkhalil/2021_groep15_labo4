package SorteerAlgo;

import java.util.Arrays;

public class InsertionSortObject implements ISortObject{


    @Override
    public void sortObject(Comparable[] array) {
        int in, out;

        for (out = 1; out < array.length; out++) {
            Comparable temp = array[out];
            in = out;

            while (in > 0 && array[in - 1].compareTo(temp) > 0) {
                array[in] = array[in - 1];
                --in;
            }
            array[in] = temp;
        }

    }

    @Override
    public void printArrayObject(Comparable[] array) {
        System.out.println(Arrays.toString(array));
    }

}
