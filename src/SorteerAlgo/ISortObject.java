package SorteerAlgo;

public interface ISortObject {
    public void sortObject(Comparable[] array);
    public void printArrayObject(Comparable[] array);
}
