package SorteerAlgo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class HelperClass {

    public int[] generateRandomArr(int lengte){
        int[] array = new int[lengte];
        for (int i =0; i < lengte; i++){
            array[i] = (int) (Math.random() * 9);
        }
        return array;
    }

    public LocalDate[] dateGenerator(int aantal) {
        LocalDate[] data = new LocalDate[aantal];
        for (int i = 0; i < aantal; i++){
            long minDay = LocalDate.of(1970, 1, 1).toEpochDay();
            long maxDay = LocalDate.of(2021, 03, 11).toEpochDay();
            long randomDay = ThreadLocalRandom.current().nextLong(minDay, maxDay);
            LocalDate randomDate = LocalDate.ofEpochDay(randomDay);
            
            data[i] = randomDate;
        }
        
        return data;

    }

    public List<String> readFile(int aantal, String fileName){
        StringBuilder sb = new StringBuilder();
        String strLine = "";
        List<String> list = new ArrayList<String>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            while (aantal > 0)
            {
                strLine = br.readLine();
                if (strLine==null){
                    br = new BufferedReader(new FileReader(fileName));
                    continue;
                }

                sb.append(strLine);
                sb.append(System.lineSeparator());
                list.add(strLine);

                aantal--;
            }
            br.close();
        } catch (FileNotFoundException e) {
            System.err.println("File not found");
        } catch (IOException e) {
            System.err.println("Unable to read the file.");
        }
        return list;
    }
}
